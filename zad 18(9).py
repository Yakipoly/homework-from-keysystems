from time import sleep


class Table:

    company = "IKEA"

    def __init__(self, material: str, size: int, color: str) -> None:
        self.material = material
        self.size = size
        self.color = color

    def get_strength(self, strength: int) -> None:
        print(strength)

    def description(self):
        return f"Table from {self.company}. Material: {self.material} with {self.size} cm"


class OakTable(Table):
    def get_strength(self, strength: int) -> None:
        print(f"Oak strength={strength}")


class AspenTable(Table):
    def get_strength(self, strength: int) -> None:
        print(f"Aspen strength={strength}")


class BirchTable(Table):
    def get_strength(self, strength: int) -> None:
        print(f"Birch strength={strength}")


class Chair:

    company = "BUBA"

    def __init__(self, material: str, size: int) -> None:
        self.material = material
        self.size = size

    def __del__(self) -> None:
        print("Вызван метод __del__()")

    def burn(self, sec: int) -> None:
        sleep(sec)
        print('It\'s burned')

    def __str__(self):
        return f"Chair from {self.company}. Material: {self.material} with {self.size} cm"




m1 = Table('Birch', 60, 'White')
m2 = Table('Oak', 55, 'Red')
m3 = Table('Aspen', 58, 'Orange')
m4 = Table('Oak', 50, 'Brown')

m1 = BirchTable('Birch', 60, 'White')
m2 = OakTable('Oak', 55, 'Red')
m3 = AspenTable('Aspen', 58, 'Orange')
m4 = OakTable('Oak', 50, 'Brown')

m1.get_strength(5)
m2.get_strength(6)




c = Chair(material='plastic', size=37)
print(c)
