"""
Напишите функцию sum_range(a, z), 
которая перемножает все целые числа от значения a до величины z включительно

"""
def sum_range(a,z):

    if a > z:
        a, z = z, a

    res = 1
    for i in range (a,z):
        res += res*i
    return res
print (sum_range(1,3))
print (sum_range(5,1))


