from time import time, sleep

def get_time(fn):
    def wrapped(s):
        start_time = time()
        res = fn(s)
        print(time()-start_time)
        return res
    return wrapped

@get_time
def polindrom(s:str)-> bool:
    '''
        полиндром
    '''
    sleep(1)
    s = s.replace(' ','')
    return s == s[::-1]

print(polindrom('на доме чемодан'))
