"""
  Напишите функцию reverse_my_list(lst), 
  которая принимает список и меняет местами его первый и последний элемент.

"""
def reverse_my_list(lst:list):
    lst[0], lst[len(lst)-1] = lst[len(lst)-1], lst[0]
    return lst
    
a=['а', 'б', 'в', 'г', 'д', 'е']
print('Вывод первонач. списка символов:\n', a)
print('Вывод результата:\n',reverse_my_list(a))



